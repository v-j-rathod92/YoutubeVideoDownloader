package com.lim.yudidownloader.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lim.yudidownloader.HomeScreenActivity;
import com.lim.yudidownloader.R;
import com.lim.yudidownloader.workers.DownloadSettings;
import com.nononsenseapps.filepicker.FilePickerActivity;

/**
 * Created by Lenovo on 03-09-2017.
 */

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener  {
    private static final int REQUEST_DOWNLOAD_PATH = 0x1235;
    private static final int REQUEST_DOWNLOAD_AUDIO_PATH = 0x1236;
    private String DOWNLOAD_PATH_PREFERENCE;
    private String DOWNLOAD_PATH_AUDIO_PREFERENCE;

    private SharedPreferences defaultPreferences;
    private Activity activity;

    public static void initSettings(Context context) {
        DownloadSettings.initSettings(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        addPreferencesFromResource(R.xml.settings);

        defaultPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        initKeys();
        updatePreferencesSummary();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        view.setBackgroundColor(ContextCompat.getColor(activity, R.color.color_background));
        return view;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {

    }

    private void initKeys() {
        DOWNLOAD_PATH_PREFERENCE = getString(R.string.download_path_key);
        DOWNLOAD_PATH_AUDIO_PREFERENCE = getString(R.string.download_path_audio_key);
    }

    private void updatePreferencesSummary() {
        findPreference(DOWNLOAD_PATH_PREFERENCE).setSummary(defaultPreferences.getString(DOWNLOAD_PATH_PREFERENCE, getString(R.string.download_path_summary)));
        findPreference(DOWNLOAD_PATH_AUDIO_PREFERENCE).setSummary(defaultPreferences.getString(DOWNLOAD_PATH_AUDIO_PREFERENCE, getString(R.string.download_path_audio_summary)));
    }

    @Override
    public void onResume() {
        super.onResume();
        defaultPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        defaultPreferences.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (HomeScreenActivity.DEBUG) Log.d("TAG", "onPreferenceTreeClick() called with: preferenceScreen = [" + preferenceScreen + "], preference = [" + preference + "]");
        if (preference.getKey().equals(DOWNLOAD_PATH_PREFERENCE) || preference.getKey().equals(DOWNLOAD_PATH_AUDIO_PREFERENCE)) {
            Intent i = new Intent(activity, FilePickerActivity.class)
                    .putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false)
                    .putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, true)
                    .putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_DIR);
            if (preference.getKey().equals(DOWNLOAD_PATH_PREFERENCE)) {
                startActivityForResult(i, REQUEST_DOWNLOAD_PATH);
            } else if (preference.getKey().equals(DOWNLOAD_PATH_AUDIO_PREFERENCE)) {
                startActivityForResult(i, REQUEST_DOWNLOAD_AUDIO_PATH);
            }
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == REQUEST_DOWNLOAD_PATH || requestCode == REQUEST_DOWNLOAD_AUDIO_PATH) && resultCode == Activity.RESULT_OK) {
            String key = getString(requestCode == REQUEST_DOWNLOAD_PATH ? R.string.download_path_key : R.string.download_path_audio_key);
            String path = data.getData().getPath();
            defaultPreferences.edit().putString(key, path).apply();
            updatePreferencesSummary();
        }
    }
}
