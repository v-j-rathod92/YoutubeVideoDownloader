package com.lim.yudidownloader.settings;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.lim.yudidownloader.R;
import com.lim.yudidownloader.util.NetworkStateUtil;

/**
 * Created by Lenovo on 03-09-2017.
 */
public class SettingActivity extends AppCompatActivity {
    private TextView txtTitle;
    private ImageView imgBack;
    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_settings);

        init();
        initializeAdvertise();
    }

    private void init() {

        adView = (AdView) findViewById(R.id.adView);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        imgBack = (ImageView) findViewById(R.id.imgBack);

        setSupportActionBar(toolbar);

        txtTitle.setText(getString(R.string.settings));
        imgBack.setVisibility(View.VISIBLE);

        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_holder, new SettingsFragment())
                .commit();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initializeAdvertise() {
        if(NetworkStateUtil.isOnline(this)){
            adView.setVisibility(View.VISIBLE);
            AdRequest adRequest = new AdRequest.Builder()
//                    .addTestDevice("C21C8AEABCB925D5933727E23B5C2567")
                    .build();
            adView.loadAd(adRequest);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (adView != null) {
            adView.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adView != null) {
            adView.destroy();
        }
    }
}
