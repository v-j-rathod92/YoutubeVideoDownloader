package com.lim.yudidownloader;

import android.app.Application;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.lim.yudidownloader.workers.DownloadSettings;

import org.schabi.newpipe.extractor.NewPipe;

/**
 * Created by Lenovo on 03-09-2017.
 */

public class App extends Application {
    private FirebaseAnalytics firebaseAnalytics;

    @Override
    public void onCreate() {
        super.onCreate();

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        NewPipe.init(com.lim.yudidownloader.workers.Downloader.getInstance());
        DownloadSettings.initSettings(this);
    }
}
