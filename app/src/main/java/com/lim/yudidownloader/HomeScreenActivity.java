package com.lim.yudidownloader;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.FloatRange;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.lim.yudidownloader.download.DownloadActivity;
import com.lim.yudidownloader.download.DownloadManagerService;
import com.lim.yudidownloader.settings.SettingActivity;
import com.lim.yudidownloader.util.NetworkStateUtil;
import com.lim.yudidownloader.util.PermissionHelper;
import com.lim.yudidownloader.util.ScreenUtil;
import com.lim.yudidownloader.util.Utils;
import com.lim.yudidownloader.workers.DownloadSettings;
import com.lim.yudidownloader.workers.StreamExtractorWorker;
import com.squareup.picasso.Picasso;

import org.schabi.newpipe.extractor.MediaFormat;
import org.schabi.newpipe.extractor.UrlIdHandler;
import org.schabi.newpipe.extractor.stream_info.AudioStream;
import org.schabi.newpipe.extractor.stream_info.StreamInfo;
import org.schabi.newpipe.extractor.stream_info.VideoStream;
import org.schabi.newpipe.extractor.youtube.YoutubeStreamUrlIdHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Vishal Rathod on 28/8/17.
 */

public class HomeScreenActivity extends AppCompatActivity implements View.OnClickListener, StreamExtractorWorker.OnStreamInfoReceivedListener,
        RadioGroup.OnCheckedChangeListener, AdapterView.OnItemSelectedListener {
    public static final boolean DEBUG = false;

    private EditText edtSearch;
    private ImageView imgSearchIcon;
    private RelativeLayout relativeLayout;
    private ValueAnimator animSearching;
    private Handler handler = new Handler();
    private Integer x;
    private boolean isSearched = false, isSearching = false;
    private int radius = ScreenUtil.dpToPx(40);

    private ArrayList<VideoStream> sortedStreamVideosList;
    private StreamInfo currentInfo;
    private int selectedVideoIndex;
    private int selectedAudioIndex;

    private ImageView imgThumbNail;
    private Button btnDownload;
    private LinearLayout layoutDownloads, layoutSettings;
    private TextView txtTitle, txtDownloads, txtSettings;

    private ScrollView scrollViewContent;
    private LinearLayout layoutDownloadInfo;
    private EditText edtFileName;
    private RadioGroup rdGroupAudioVideo;
    private Spinner spinnerFormat;
    private SeekBar seekBarThreads;
    private TextView txtFileName, txtType, txtFormat, txtThread;
    private TextView txtDescription, txtVideoInfo;

    private SharedPreferences mPrefs;
    private int defThread = 3;
    private Typeface fontSegoe, fontSegoeLight;
    private AdView adView;
    private InterstitialAd mInterstitialAd;
    private AdRequest adRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_home_screen);

        init();
        setListeners();
        setTypeface();
    }

    private void init() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#44440f0f"));
        }

        fontSegoe = Typeface.createFromAsset(this.getResources().getAssets(), "fonts/" + getString(R.string.font_segoe));
        fontSegoeLight = Typeface.createFromAsset(this.getResources().getAssets(), "fonts/" + getString(R.string.font_segoe_light));

        edtSearch = (EditText) findViewById(R.id.edtSearch);
        imgSearchIcon = (ImageView) findViewById(R.id.imgSearchIcon);
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);

        imgThumbNail = (ImageView) findViewById(R.id.imgThumbNail);
        txtVideoInfo = (TextView) findViewById(R.id.txtVideoInfo);
        btnDownload = (Button) findViewById(R.id.btnDownload);
        layoutDownloads = (LinearLayout) findViewById(R.id.layoutDownloads);
        layoutSettings = (LinearLayout) findViewById(R.id.layoutSettings);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtDownloads = (TextView) findViewById(R.id.txtDownloads);
        txtSettings = (TextView) findViewById(R.id.txtSettings);
        txtDescription = (TextView) findViewById(R.id.txtDescription);

        scrollViewContent = (ScrollView) findViewById(R.id.scrollViewContent);
        layoutDownloadInfo = (LinearLayout) findViewById(R.id.layoutDownloadInfo);
        edtFileName = (EditText) findViewById(R.id.edtFileName);
        rdGroupAudioVideo = (RadioGroup) findViewById(R.id.rdGroupAudioVideo);
        spinnerFormat = (Spinner) findViewById(R.id.spinnerFormat);
        txtThread = (TextView) findViewById(R.id.txtThread);
        seekBarThreads = (SeekBar) findViewById(R.id.seekBarThreads);
        txtFileName = (TextView) findViewById(R.id.txtFileName);
        txtType = (TextView) findViewById(R.id.txtType);
        txtFormat = (TextView) findViewById(R.id.txtFormat);
        txtThread = (TextView) findViewById(R.id.txtThread);
        adView = (AdView) findViewById(R.id.adView);

        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        txtDescription.setAutoLinkMask(Linkify.WEB_URLS);
        txtDescription.setMovementMethod(LinkMovementMethod.getInstance());

        relativeLayout.setX((ScreenUtil.getScreenWidth() / 2) - (radius / 2));

        txtThread.setText("Thread - " + String.valueOf(defThread));
        seekBarThreads.setProgress(defThread - 1);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                animateSearchEditText();
            }
        }, 200);

    }

    private void setListeners() {
        imgSearchIcon.setOnClickListener(this);
        btnDownload.setOnClickListener(this);
        layoutDownloads.setOnClickListener(this);
        layoutSettings.setOnClickListener(this);

        rdGroupAudioVideo.setOnCheckedChangeListener(this);
        spinnerFormat.setOnItemSelectedListener(this);

        seekBarThreads.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, final int progress, boolean fromUser) {
                Log.e("progress", progress + "");

                txtThread.post(new Runnable() {
                    @Override
                    public void run() {
                        txtThread.setText("Thread - " + (progress + 1));
                    }
                });
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void setTypeface() {
        edtSearch.setTypeface(fontSegoeLight);
        txtTitle.setTypeface(fontSegoe);
        txtDownloads.setTypeface(fontSegoeLight);
        txtSettings.setTypeface(fontSegoeLight);

        txtFileName.setTypeface(fontSegoe);
        txtType.setTypeface(fontSegoe);
        txtFormat.setTypeface(fontSegoe);
        txtThread.setTypeface(fontSegoe);
        txtVideoInfo.setTypeface(fontSegoe);
        txtDescription.setTypeface(fontSegoe);
    }

    private void animateSearchEditText() {

        if (scrollViewContent.getVisibility() == View.VISIBLE) {
            txtTitle.setVisibility(View.GONE);
        } else {
            txtTitle.setVisibility(View.VISIBLE);
        }
        // move center to left of screen
        final ValueAnimator valueAnimator = ValueAnimator.ofFloat(relativeLayout.getX(), ScreenUtil.dpToPx(10));
        valueAnimator.setDuration(300);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Float position = (Float) animation.getAnimatedValue();
                relativeLayout.setX(position);
            }
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Log.e("move left end", "ok");

                imgSearchIcon.setLayoutParams(getImageViewCenterParams());
            }
        });

        // make with match parent
        ValueAnimator widthAnimation = ValueAnimator.ofInt(radius,
                ScreenUtil.getScreenWidth() - ScreenUtil.dpToPx(10) - ScreenUtil.dpToPx(10));
        widthAnimation.setDuration(300);
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {

                Integer width = (Integer) animation.getAnimatedValue();
                RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(width, radius);
                param.addRule(RelativeLayout.CENTER_VERTICAL);
                param.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

                relativeLayout.setLayoutParams(param);

            }
        });


        // choreograph animation
        AnimatorSet anim = new AnimatorSet();
        anim.play(valueAnimator).before(widthAnimation);
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                // zoom in out animation
                imgSearchIcon.animate().scaleX(0).scaleY(0).setDuration(300).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        imgSearchIcon.setImageResource(R.drawable.go);
                        imgSearchIcon.animate().scaleX(1).scaleY(1).setDuration(300).start();
//                        first time ad load
                        if (adRequest == null) {
                            initializeAdvertise();
                        }
                    }
                }).start();
            }
        });
        anim.start();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgSearchIcon:
                if (!isSearching) {
                    if (isSearched) {
                        isSearched = false;
                        edtSearch.setVisibility(View.VISIBLE);
                        edtSearch.setEnabled(true);
                        animateSearchEditText();
                    } else {
                        if (validate()) {
                            isSearching = true;
                            startSearching();
                            animateGo();
                        }
                    }
                }

                break;

            case R.id.btnDownload:
                if (layoutDownloadInfo.getVisibility() == View.GONE) {
                    displayDownloadInfo();
                } else {
                    if (!PermissionHelper.checkStoragePermissions(this)) {
                        return;
                    }

                    startDownloading();
                }

                break;

            case R.id.layoutDownloads:
                if (!PermissionHelper.checkStoragePermissions(this)) {
                    return;
                }
                startActivity(new Intent(this, DownloadActivity.class));
                break;

            case R.id.layoutSettings:
                startActivity(new Intent(this, SettingActivity.class));
                break;
        }
    }

    private void startSearching() {
        String url = edtSearch.getText().toString().trim();
        StreamExtractorWorker curExtractorWorker = new StreamExtractorWorker(this, 0, url, this);
        curExtractorWorker.start();
    }

    private boolean validate() {
        UrlIdHandler urlIdHandler = YoutubeStreamUrlIdHandler.getInstance();

        if (edtSearch.getText().toString().trim().length() == 0) {
            displyError(getString(R.string.enter_url));
            return false;
        } else if (!urlIdHandler.acceptUrl(edtSearch.getText().toString())) {
            displyError(getString(R.string.invalid_url));
            return false;
        }
        return true;
    }

    private void animateGo() {
        final int left = edtSearch.getLeft();
        int right = edtSearch.getRight();
        final int middle = (left + right) / 2;

        edtSearch.setEnabled(false);
//        imgSearchIcon.setPadding(0, 0, 0, 0);
        imgSearchIcon.setImageResource(R.drawable.search);

        // shrink search bar animation
        ValueAnimator shrinkAnim = ValueAnimator.ofInt(edtSearch.getHeight(), 3);
        shrinkAnim.setDuration(300);
        shrinkAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer integer = (Integer) animation.getAnimatedValue();

                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) edtSearch.getLayoutParams();
                layoutParams.height = ScreenUtil.dpToPx(integer);
                edtSearch.setLayoutParams(layoutParams);
            }
        });

        shrinkAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                // animate search icon while progress
                animSearching = ValueAnimator.ofInt((int) imgSearchIcon.getX(), left);
                animSearching.setRepeatCount(ValueAnimator.INFINITE);
                animSearching.setRepeatMode(ValueAnimator.REVERSE);
                animSearching.setDuration(1000);
                animSearching.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        x = (Integer) animation.getAnimatedValue();
                        imgSearchIcon.setX(x);
                    }
                });


                animSearching.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(final Animator animation) {
                        super.onAnimationEnd(animation);

                        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

                        // stop searching to middle of screen with animation
                        imgSearchIcon.animate().x(middle - ScreenUtil.dpToPx(25)).setDuration(1000).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                // animate check mark animation
                                imgSearchIcon.setImageResource(R.drawable.animated_star);
                                Drawable drawable = imgSearchIcon.getDrawable();
                                if (drawable instanceof Animatable) {
                                    ((Animatable) drawable).start();

                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                            // expand search edittext animation
                                            imgSearchIcon.setImageResource(R.drawable.search);
                                            ValueAnimator expandAnim = ValueAnimator.ofInt(edtSearch.getHeight(), radius);
                                            expandAnim.setDuration(300);
                                            expandAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                @Override
                                                public void onAnimationUpdate(ValueAnimator animation) {
                                                    Integer integer = (Integer) animation.getAnimatedValue();

                                                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) edtSearch.getLayoutParams();
                                                    layoutParams.height = ScreenUtil.dpToPx(integer);
                                                    edtSearch.setLayoutParams(layoutParams);
                                                }
                                            });

                                            // width wrapping animation
                                            ValueAnimator widthAnim = ValueAnimator.ofInt(edtSearch.getWidth(), radius);
                                            widthAnim.setDuration(200);
                                            widthAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                @Override
                                                public void onAnimationUpdate(ValueAnimator animation) {
                                                    Integer integer = (Integer) animation.getAnimatedValue();

                                                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) relativeLayout.getLayoutParams();
                                                    layoutParams.width = integer;
                                                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                                                    relativeLayout.setLayoutParams(layoutParams);

                                                    int w = relativeLayout.getWidth();
                                                    int middle = (w / 2) - (imgSearchIcon.getWidth() / 2);
                                                    imgSearchIcon.setX(middle);
                                                }
                                            });

                                            AnimatorSet animatorSet = new AnimatorSet();
                                            animatorSet.play(expandAnim).with(widthAnim);
                                            animatorSet.addListener(new AnimatorListenerAdapter() {
                                                @Override
                                                public void onAnimationEnd(Animator animation) {
                                                    super.onAnimationEnd(animation);

                                                    // goto title bar animation
                                                    edtSearch.setVisibility(View.GONE);
                                                    imgSearchIcon.setLayoutParams(getImageViewCenterParams());
                                                    int y = toolbar.getHeight() - relativeLayout.getHeight();
                                                    relativeLayout.animate().x(toolbar.getWidth() - relativeLayout.getWidth()).y(y / 2).withEndAction(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            txtTitle.setVisibility(View.VISIBLE);
                                                            scrollViewContent.setVisibility(View.VISIBLE);

                                                            if (adView.getVisibility() == View.VISIBLE) {
                                                                RelativeLayout.LayoutParams layoutParams = new
                                                                        RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                                                        ViewGroup.LayoutParams.WRAP_CONTENT);
                                                                layoutParams.addRule(RelativeLayout.BELOW, R.id.adView);
                                                                layoutParams.addRule(RelativeLayout.ABOVE, R.id.layoutBottomMenu);
                                                                layoutParams.setMargins(ScreenUtil.dpToPx(10), ScreenUtil.dpToPx(10),
                                                                        ScreenUtil.dpToPx(10), 0);
                                                                scrollViewContent.setLayoutParams(layoutParams);
                                                            }
                                                            isSearching = false;
                                                            showContentWithAnimation(300, 0, 0);
                                                            showFullScreenAdIfNeeded();
                                                        }
                                                    }).setDuration(500).start();
                                                }
                                            });
                                            animatorSet.start();
                                        }
                                    }, 1000);
                                }
                            }
                        }).start();
                    }
                });
                animSearching.start();
            }
        });

        shrinkAnim.start();
    }

    @Override
    public void onReceive(StreamInfo info) {
        Log.e("StreamInfo", "ok");

        this.currentInfo = info;

        isSearched = true;

        edtSearch.setText("");
        animSearching.cancel();

        sortedStreamVideosList = Utils.getSortedStreamVideosList(this, info.video_streams, info.video_only_streams, false);
    }

    @Override
    public void onError(int messageId) {
        Log.e("error", messageId + "");
    }

    @Override
    public void onReCaptchaException() {
        Log.e("recaptcha", "ok");
    }

    @Override
    public void onBlockedByGemaError() {
        Log.e("onblocked", "ok");
    }

    @Override
    public void onContentErrorWithMessage(int messageId) {
        Log.e("onContentErrorWithMessage", "ok");
    }

    @Override
    public void onContentError() {
        Log.e("onContentError", "ok");
    }

    @Override
    public void onUnrecoverableError(Exception exception) {
        Log.e("onUnrecoverableError", "ok");
    }

    private RelativeLayout.LayoutParams getImageViewCenterParams() {
        RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) imgSearchIcon.getLayoutParams();
        param.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        param.addRule(RelativeLayout.CENTER_VERTICAL);
        return param;
    }

    private void displayDownloadInfo() {
        layoutDownloadInfo.setVisibility(View.VISIBLE);
        edtFileName.setText(createFileName(currentInfo.title));

        setupVideoSpinner(sortedStreamVideosList);
    }

    public Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.rdBtnVideo:
                setupVideoSpinner(sortedStreamVideosList);
                break;

            case R.id.rdBtnAudio:
                setupAudioSpinner(currentInfo.audio_streams);
                break;
        }
    }

    private void setupVideoSpinner(List<VideoStream> videoStreams) {
        spinnerFormat.setAdapter(new SpinnerToolbarAdapter(this, videoStreams, true));
    }

    private void setupAudioSpinner(List<AudioStream> audioStreams) {
        String[] items = new String[audioStreams.size()];

        for (int i = 0; i < audioStreams.size(); i++) {
            AudioStream audioStream = audioStreams.get(i);
            items[i] = MediaFormat.getNameById(audioStream.format) + " " + audioStream.avgBitrate + "kbps";
        }

        ArrayAdapter<String> itemAdapter = new ArrayAdapter<>(HomeScreenActivity.this, android.R.layout.simple_spinner_dropdown_item, items);
        spinnerFormat.setAdapter(itemAdapter);
    }

    /**
     * #143 #44 #42 #22: make sure that the filename does not contain illegal chars.
     * This should fix some of the "cannot download" problems.
     */
    private String createFileName(String fileName) {
        // from http://eng-przemelek.blogspot.de/2009/07/how-to-create-valid-file-name.html

        List<String> forbiddenCharsPatterns = new ArrayList<>();
        forbiddenCharsPatterns.add("[:]+"); // Mac OS, but it looks that also Windows XP
        forbiddenCharsPatterns.add("[\\*\"/\\\\\\[\\]\\:\\;\\|\\=\\,]+");  // Windows
        forbiddenCharsPatterns.add("[^\\w\\d\\.]+");  // last chance... only latin letters and digits
        String nameToTest = fileName;
        for (String pattern : forbiddenCharsPatterns) {
            nameToTest = nameToTest.replaceAll(pattern, "_");
        }
        return nameToTest;
    }

    private void startDownloading() {
        String url, location;

        String fileName = edtFileName.getText().toString().trim();
        if (fileName.isEmpty()) fileName = createFileName(currentInfo.title);

        boolean isAudio = rdGroupAudioVideo.getCheckedRadioButtonId() == R.id.rdBtnAudio;
        url = isAudio ? currentInfo.audio_streams.get(selectedAudioIndex).url : sortedStreamVideosList.get(selectedVideoIndex).url;
        location = isAudio ? DownloadSettings.getAudioDownloadPath(this) : DownloadSettings.getVideoDownloadPath(this);

        if (isAudio)
            fileName += "." + MediaFormat.getSuffixById(currentInfo.audio_streams.get(selectedAudioIndex).format);
        else
            fileName += "." + MediaFormat.getSuffixById(sortedStreamVideosList.get(selectedVideoIndex).format);

        Log.e("url", url);
        Log.e("location", location);
        Log.e("fileName", fileName);
        Log.e("isAudio", isAudio + "");
        Log.e("thread count", (seekBarThreads.getProgress() + 1) + "");

        DownloadManagerService.startMission(this, url, location, fileName, isAudio, seekBarThreads.getProgress() + 1);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        switch (rdGroupAudioVideo.getCheckedRadioButtonId()) {
            case R.id.rdBtnAudio:
                selectedAudioIndex = position;
                break;
            case R.id.rdBtnVideo:
                selectedVideoIndex = position;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void showContentWithAnimation(long duration, long delay, @FloatRange(from = 0.0f, to = 1.0f) float translationPercent) {
        int translationY = (int) (getResources().getDisplayMetrics().heightPixels *
                (translationPercent > 0.0f ? translationPercent : .12f));

        scrollViewContent.scrollTo(0, 0);
        layoutDownloadInfo.setVisibility(View.GONE);


        imgThumbNail.setVisibility(View.GONE);
        btnDownload.setVisibility(View.GONE);
        txtVideoInfo.setVisibility(View.GONE);
        txtDescription.setVisibility(View.GONE);

        Picasso.with(this).load(currentInfo.thumbnail_url).placeholder(R.drawable.video_placeholder).
                into((ImageView) findViewById(R.id.imgThumbNail));

        String strInfo = "Title :- " + currentInfo.title + "\n"
                + "Uploader :- " + currentInfo.uploader + "\n"
                + "Total view :- " + currentInfo.view_count + "\n"
                + "Dislikes :- " + currentInfo.dislike_count + "\n"
                + "Likes :- " + currentInfo.like_count + "\n"
                + "Duration : " + getDurationString(currentInfo.duration) + "\n"
                + "Upload date : " + currentInfo.upload_date;

        Log.e("info", strInfo);
        txtVideoInfo.setText(strInfo);

        imgThumbNail.animate().setListener(null).cancel();
        imgThumbNail.setAlpha(0f);
        imgThumbNail.setTranslationY(translationY);
        imgThumbNail.setVisibility(View.VISIBLE);
        imgThumbNail.animate().alpha(1f).translationY(0)
                .setStartDelay(delay).setDuration(duration).setInterpolator(new FastOutSlowInInterpolator()).start();

        btnDownload.animate().setListener(null).cancel();
        btnDownload.setAlpha(0f);
        btnDownload.setTranslationY(translationY);
        btnDownload.setVisibility(View.VISIBLE);
        btnDownload.animate().alpha(1f).translationY(0)
                .setStartDelay((long) (duration * .5f) + delay).setDuration(duration).setInterpolator(new FastOutSlowInInterpolator()).start();


        txtVideoInfo.animate().setListener(null).cancel();
        txtVideoInfo.setAlpha(0f);
        txtVideoInfo.setTranslationY(translationY);
        txtVideoInfo.setVisibility(View.VISIBLE);
        txtVideoInfo.animate().alpha(1f).translationY(0)
                .setStartDelay((long) (duration * .8f) + delay).setDuration(duration).setInterpolator(new FastOutSlowInInterpolator()).start();

        if (currentInfo.description != null && !currentInfo.description.equals("")) {
            txtDescription.setText("Description : " + fromHtml(currentInfo.description));

            txtDescription.animate().setListener(null).cancel();
            txtDescription.setAlpha(0f);
            txtDescription.setTranslationY(translationY);
            txtDescription.setVisibility(View.VISIBLE);
            txtDescription.animate().alpha(1f).translationY(0)
                    .setStartDelay((long) (duration * 1.1f) + delay).setDuration(duration).setInterpolator(new FastOutSlowInInterpolator()).start();
        }
    }

    public static String getDurationString(int duration) {
        if (duration < 0) {
            duration = 0;
        }
        String output;
        int days = duration / (24 * 60 * 60); /* greater than a day */
        duration %= (24 * 60 * 60);
        int hours = duration / (60 * 60); /* greater than an hour */
        duration %= (60 * 60);
        int minutes = duration / 60;
        int seconds = duration % 60;

        //handle days
        if (days > 0) {
            output = String.format(Locale.US, "%d:%02d:%02d:%02d", days, hours, minutes, seconds);
        } else if (hours > 0) {
            output = String.format(Locale.US, "%d:%02d:%02d", hours, minutes, seconds);
        } else {
            output = String.format(Locale.US, "%d:%02d", minutes, seconds);
        }
        return output;
    }

    private void displyError(String error) {
        Snackbar snackbar;
        snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout), error, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
//        snackBarView.setBackgroundColor(Color.RED);
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (adView != null) {
            adView.pause();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adView != null) {
            adView.destroy();
        }
    }

    private void initializeAdvertise() {
        if (NetworkStateUtil.isOnline(this)) {
            adRequest = new AdRequest.Builder()
//                    .addTestDevice("C21C8AEABCB925D5933727E23B5C2567")
                    .build();
            AdListener adListener = new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    adView.setVisibility(View.VISIBLE);
                }
            };
            adView.setAdListener(adListener);
            adView.loadAd(adRequest);

            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.admob_full_screen_ad));
            mInterstitialAd.loadAd(adRequest);
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();

                    AdRequest adRequest = new AdRequest.Builder()
//                            .addTestDevice("C21C8AEABCB925D5933727E23B5C2567")
                            .build();
                    mInterstitialAd.loadAd(adRequest);
                }
            });
        }
    }

    private void showFullScreenAdIfNeeded() {
        int totalSearched = mPrefs.getInt("total_searched", 0);
        totalSearched = totalSearched + 1;

        if (totalSearched == 5) {
            mPrefs.edit().putInt("total_searched", 0).commit();

            // use delay for letting animation of searched video content
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayFullScreenAd();
                }
            }, 1000);
        } else {
            mPrefs.edit().putInt("total_searched", totalSearched).commit();
        }
    }

    private void displayFullScreenAd() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }
}
