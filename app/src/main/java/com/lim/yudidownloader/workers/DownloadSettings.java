package com.lim.yudidownloader.workers;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.lim.yudidownloader.R;

import java.io.File;

/**
 * Created by Vishal Rathod on 30/8/17.
 */

public class DownloadSettings {

    public static void initSettings(Context context) {
        PreferenceManager.setDefaultValues(context, R.xml.settings, false);
        getVideoDownloadFolder(context);
        getAudioDownloadFolder(context);
    }

    public static File getVideoDownloadFolder(Context context) {
        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES);
        return getFolder(context, R.string.download_path_key, path.getPath());
    }

    public static String getVideoDownloadPath(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final String key = context.getString(R.string.download_path_key);
        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES);

        return prefs.getString(key, path.getPath());
    }

    public static File getAudioDownloadFolder(Context context) {
        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MUSIC);
        return getFolder(context, R.string.download_path_audio_key, path.getPath());
    }

    public static String getAudioDownloadPath(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final String key = context.getString(R.string.download_path_audio_key);
        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MUSIC);

        return prefs.getString(key, path.getPath());
    }

    private static File getFolder(Context context, int keyID, String defaultDirectoryName) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final String key = context.getString(keyID);
        String downloadPath = prefs.getString(key, null);
        if ((downloadPath != null) && (!downloadPath.isEmpty())) return new File(downloadPath.trim());

//        final File folder = getFolder(defaultDirectoryName);
        File folder = new File(defaultDirectoryName);
        SharedPreferences.Editor spEditor = prefs.edit();
        spEditor.putString(key
                , new File(folder,context.getResources().getString(R.string.app_name)).getAbsolutePath());
        spEditor.apply();
        return folder;
    }

    @NonNull
    private static File getFolder(String defaultDirectoryName) {
        return new File(Environment.getExternalStorageDirectory(),defaultDirectoryName);
    }
}
