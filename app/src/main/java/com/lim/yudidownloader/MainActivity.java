package com.lim.yudidownloader;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.lim.yudidownloader.util.Utils;
import com.lim.yudidownloader.workers.StreamExtractorWorker;
import com.squareup.picasso.Picasso;

import org.schabi.newpipe.extractor.NewPipe;
import org.schabi.newpipe.extractor.stream_info.StreamInfo;
import org.schabi.newpipe.extractor.stream_info.VideoStream;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, StreamExtractorWorker.OnStreamInfoReceivedListener {
    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0";
    private EditText edtUrl;
    private Button btnInfo;
    private TextView txtVideoInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        setListeners();
    }

    private void init() {
        edtUrl = (EditText) findViewById(R.id.edtUrl);
        btnInfo = (Button) findViewById(R.id.btnInfo);
        txtVideoInfo = (TextView) findViewById(R.id.txtVideoInfo);
    }

    private void setListeners() {
        btnInfo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInfo:
                getYoutubeVideoInfo();
                break;
        }
    }

    private void getYoutubeVideoInfo() {
        String url = edtUrl.getText().toString().trim();
        if (url.length() < 1) {
            return;
        }

        NewPipe.init(com.lim.yudidownloader.workers.Downloader.getInstance());
        StreamExtractorWorker curExtractorWorker = new StreamExtractorWorker(this, 0, url, this);
        curExtractorWorker.start();

//        try {
//            StreamingService youtubeService = NewPipe.getService(0);
//            StreamExtractor extractor = youtubeService.getExtractorInstance(url);
//
//            StreamInfo videoStreamInfo = StreamInfo.getVideoInfo(extractor);
//
//            for (VideoStream stream : videoStreamInfo.video_streams) {
//                Log.d("stream", stream.resolution);
//                Log.e("url", stream.url);
//            }
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onReceive(StreamInfo info) {
        Log.e("StreamInfo", "ok");
        ArrayList<VideoStream> sortedStreamVideosList = Utils.getSortedStreamVideosList(this, info.video_streams, info.video_only_streams, false);

        for (VideoStream stream : info.video_streams) {
            if (stream.resolution.contains("320p") ||
                    stream.resolution.contains("720p") ||
                    stream.resolution.contains("360p")) {

                Log.e("url", info.webpage_url);
            }
        }

        String strInfo = "Title :- " + info.title + "\n"
                + "Uploader :- " + info.uploader
                + "Total view :- " + info.view_count
                + "Dislikes :- " + info.dislike_count
                + "Likes :- " + info.like_count
                + "Duration : " + info.duration
                + "Upload date : " + info.upload_date
                + "Description : " + info.description;
        txtVideoInfo.setText(strInfo);

        Picasso.with(this).load(info.thumbnail_url).into((ImageView)findViewById(R.id.img));


    }

    @Override
    public void onError(int messageId) {
        Log.e("onError", "ok");
    }

    @Override
    public void onReCaptchaException() {
        Log.e("onReCaptchaException", "ok");
    }

    @Override
    public void onBlockedByGemaError() {
        Log.e("onBlockedByGemaError", "ok");
    }

    @Override
    public void onContentErrorWithMessage(int messageId) {
        Log.e("onContentErrorWithMessage", "ok");
    }

    @Override
    public void onContentError() {
        Log.e("onContentError", "ok");
    }

    @Override
    public void onUnrecoverableError(Exception exception) {
        Log.e("onUnrecoverableError", "ok");
    }
}
