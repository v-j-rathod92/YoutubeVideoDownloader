package com.lim.yudidownloader.download;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.lim.yudidownloader.R;
import com.lim.yudidownloader.get.DownloadManager;
import com.lim.yudidownloader.util.NetworkStateUtil;

/**
 * Created by Vishal Rathod on 30/8/17.
 */

public class DownloadActivity extends AppCompatActivity {
    private DownloadManager mManager;
    private DownloadManagerService.DMBinder mBinder;

    private SharedPreferences mPrefs;
    private boolean mLinear;
    private MenuItem mSwitch;

    private TextView txtTitle;
    private ImageView imgBack;
    private RecyclerView mList;
    private MissionAdapter mAdapter;
    private GridLayoutManager mGridManager;
    private LinearLayoutManager mLinearManager;
    private AdView adView;
    private LinearLayout layoutEmpty;
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            mBinder = (DownloadManagerService.DMBinder) binder;
            mManager = mBinder.getDownloadManager();

            if(mManager.getCount() == 0){
                displayEmptyView();
            }
            else{
                updateList();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // What to do?
        }


    };

    public void displayEmptyView(){
        mList.setVisibility(View.GONE);
        layoutEmpty.setVisibility(View.VISIBLE);
    }

    private void updateList() {
        mAdapter = new MissionAdapter(this, mBinder, mManager, mLinear);

        if (mLinear) {
            mList.setLayoutManager(mLinearManager);
        } else {
            mList.setLayoutManager(mGridManager);
        }

        mList.setAdapter(mAdapter);

        if (mSwitch != null) {
            mSwitch.setIcon(mLinear ? R.drawable.grid : R.drawable.list);
        }

        mPrefs.edit().putBoolean("linear", mLinear).commit();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_download);

        init();
        initializeAdvertise();
    }


    private void init() {
        Intent i = new Intent();
        i.setClass(this, DownloadManagerService.class);
        startService(i);
        bindService(i, mConnection, Context.BIND_AUTO_CREATE);

        adView = (AdView) findViewById(R.id.adView);
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        mLinear = mPrefs.getBoolean("linear", false);


        // Views
        layoutEmpty = (LinearLayout) findViewById(R.id.layoutEmpty);
        mList = (RecyclerView) findViewById(R.id.recyclerDownload);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        imgBack = (ImageView) findViewById(R.id.imgBack);

        txtTitle.setText(getString(R.string.downloads));
        imgBack.setVisibility(View.VISIBLE);
        // Init
        mGridManager = new GridLayoutManager(this, 2);
        mLinearManager = new LinearLayoutManager(this);
        mList.setLayoutManager(mGridManager);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initializeAdvertise() {
        if(NetworkStateUtil.isOnline(this)){
            AdRequest adRequest = new AdRequest.Builder()
//                    .addTestDevice("C21C8AEABCB925D5933727E23B5C2567")
                    .build();
            AdListener adListener = new AdListener(){
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    adView.setVisibility(View.VISIBLE);
                }
            };
            adView.setAdListener(adListener);
            adView.loadAd(adRequest);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (adView != null) {
            adView.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adView != null) {
            adView.destroy();
        }
        unbindService(mConnection);
    }
}
